pipeline {
    agent {
        label "master"
    }
   
    stages {
        stage("clone from git") {
            steps {
                script {
                  
                                        git branch: 'master', url: 'https://gitlab.com/sabrine.melki/reservation-cd.git';
                }
            }
        }

        stage("mvn clean") {
            steps {
                script {
                    bat "mvn clean"
                }
            }
        }

        stage("mvn package") {
            steps {
                script {
                    bat "mvn package -DskipTests=true"
                }
            }
        }

         stage("mvn docker image push") {
            steps {
                script {
                bat "docker login  -u sabrinemelki -p smsforyou23*"
                    bat "mvn packagedockerfile:push"
                }
            }
        }
}
}